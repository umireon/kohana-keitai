<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

	'mapping' => array(
		'docomo'     => 'docomo',
		'up.browser' => 'au',
		'vodafone'   => 'softbank',
		'softbank'   => 'softbank',
	),

);
