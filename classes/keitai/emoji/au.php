<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Emoji_Au extends Emoji {

	public $tags = array(
		'accesskey' => '<img localsrc="%d"/>',
	);

	public function accesskey($name)
	{
		if (strcmp($name, '0') == 0)
		{
			$eid = 325;
		}
		elseif (strcmp($name, '1') >= 0 and strcmp($name, '9') <= 0)
		{
			$eid = 179 + (int) $name;
		}
		else
		{
			return parent::accesskey($name);
		}

		return sprintf($this->tags['accesskey'], $eid);
	}

} // End Au Emoji