<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Emoji_DocomoSjis extends Emoji {

	public function accesskey($name)
	{
		if (strcmp($name, '0') === 0)
		{
			$char = 0xF990;
		}
		elseif (strcmp($name, '1') >= 0 and strcmp($name, '9') <= 0)
		{
			$char = 0xF986 + (int) $name;
		}
		else
		{
			return parent::accesskey($name);
		}

		return pack('n', $char);
	}

} // End Docomo Emoji