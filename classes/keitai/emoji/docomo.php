<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Emoji_Docomo extends Emoji {

	public function accesskey($name)
	{
		if (strcmp($name, '0') === 0)
		{
			$code = 0xE6EB;
		}
		elseif (strcmp($name, '1') >= 0 and strcmp($name, '9') <= 0)
		{
			$code = 0xE6E1 + (int) $name;
		}
		else
		{
			return parent::accesskey($name);
		}

		return '&#x'.dechex($code).';';
	}

} // End Docomo Emoji