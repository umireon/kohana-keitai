<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Emoji_Softbank extends Emoji {

	public function accesskey($name)
	{
		if (strcmp($name, '0') === 0)
		{
			$char = 'E225';
		}
		elseif (strcmp($name, '1') >= 0 and strcmp($name, '9') <= 0)
		{
			$char = strtoupper(dechex(0xE21B + (int) $name));
		}
		else
		{
			return parent::accesskey($name);
		}

		return "&#x{$char};";
	}

} // End Softbank Emoji