<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Emoji {

	public static $instance;

	public function accesskey($name)
	{
		return '['.HTML::chars($name).']';
	}

	public static function factory($name)
	{
		$class = 'Emoji_'.$name;
		$instance = new $class;

		if (empty(Emoji::$instance))
		{
			Emoji::$instance = $instance;
		}

		return $instance;
	}

	public static function instance()
	{
		return Emoji::$instance;
	}

} // End Emoji