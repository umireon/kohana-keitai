<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Carrier_Docomo extends Keitai {

	protected $_encoding      = 'Shift_JIS';
	protected $_short_encname = 'sjis';
	protected $_mb_encname    = 'SJIS-win';

	public function create_emoji()
	{
		return Emoji::factory('Docomo');
	}

} // End Docomo Keitai