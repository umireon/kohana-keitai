<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Carrier_Au extends Keitai {

	protected $_encoding      = 'Shift_JIS';
	protected $_short_encname = 'sjis';
	protected $_mb_encname    = 'SJIS-win';

	public function reload($title, array $attributes = NULL)
	{
		$query = array('force_reload' => uniqid()) + $_GET;
		$uri = Request::initial()->uri().'?'.http_build_query($query, NULL, '&');

		return Keitai::anchor($uri, $title, $attributes);
	}

	public function create_emoji()
	{
		return $this->emoji = Emoji::factory('Au');
	}

} // End Au Keitai
