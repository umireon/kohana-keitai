<?php defined('SYSPATH') or die('No direct access allowed.');

class Keitai_Carrier_Softbank extends Keitai {

	protected $_encoding = 'UTF-8';

	public function create_emoji()
	{
		return Emoji::factory('Softbank');
	}

} // End Softbank Keitai