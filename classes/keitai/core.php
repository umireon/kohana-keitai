<?php defined('SYSPATH') or die('No direct access allowed.');

abstract class Keitai_Core {

	protected static $_device_type;
	protected static $_instance;
	
	public static function init()
	{
		$config = Kohana::config('keitai');
		$type   = self::device_type($config);

		if ($type) {
			$class = 'Keitai_'.$type;
			self::$_instance = new $class($config);
			self::$_instance->on_init();
		}
		else
		{
			self::$_instance = NULL;
		}
	}

	public static function instance()
	{
		return self::$_instance;
	}

	protected static function device_type($config)
	{
		if (isset(self::$_device_type))
		{
			return self::$_device_type;
		}

		if (isset($_SERVER['HTTP_USER_AGENT']))
		{
			$agent   = $_SERVER['HTTP_USER_AGENT'];
			$mapping = $config['mapping'];

			foreach ($mapping as $str => $type)
			{
				if (stripos($agent, $str) !== FALSE)
				{
					return self::$_device_type = $type;
				}
			}
		}

		return self::$_device_type = FALSE;
	}

	public $emoji;

	protected $_activated = FALSE;
	protected $_config;

	public function __construct($config = array())
	{
		$this->_config = $config;
		$this->emoji = $this->create_emoji();
	}

	public function anchor($uri, $title = NULL, array $attributes = array(), $protocol = NULL, $index = FALSE)
	{
		if ($this->session_active())
		{
			$uri = $this->append_sid($uri);
		}

		$anchor = HTML::anchor($uri, $title, $attributes, $protocol, $index);

		if (isset($attributes['accesskey']))
		{
			return $this->emoji->accesskey($attributes['accesskey']).$anchor;
		}
		else
		{
			return $anchor;
		}
	}

	public function reload($title, array $attributes = NULL)
	{
		$uri = Request::current()->uri();
		unset($_GET[session_name()]);
		if (! empty($_GET)) {
			$uri .= '?'.http_build_query($_GET, NULL, '&');
		}
		$_GET[session_name()] = session_id();

		return Keitai::anchor($uri, $title, $attributes);
	}

	public function redirect($url = '', $code = 302)
	{
		if ($this->session_active())
		{
			$url = $this->append_sid($url);
		}

		Request::initial()->redirect($url, $code);
	}

	protected function append_sid($uri)
	{
		if (strpos('?', $uri) !== FALSE)
		{
			$uri .= '&';
		}
		else
		{
			$uri .= '?';
		}

		$uri .= $this->sid();
		return $uri;
	}

	protected function session_active()
	{
		return FALSE;//Auth::instance()->logged_in();
	}

	protected function sid()
	{
		$s = Session::instance();
		return $s->name().'='.$s->id();
	}

	abstract public function create_emoji();

	public function on_init() {
		//Session::$default = 'keitai';
		//ini_set('session.use_only_cookies', 'Off');
	}

} // End Keitai